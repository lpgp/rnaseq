#!/usr/bin/env nextflow

// fastqc
process FASTQC {

	publishDir "${params.out_dir}/fastqc", mode: 'copy', overwrite: true

	input:
	tuple val(replicateId), path(reads) 

	output:
	path '*.html'

	script:
	"""
	conda run -n env fastqc -t 2 ${reads}
	"""
}

// read trimming
process TRIM {

	publishDir "${params.out_dir}/trimfastq", mode: 'symlink', overwrite: true

	input:
	tuple val(replicateId), path(reads) 

	output:
	tuple val(replicateId), path('*.fq.gz') 

	script:
 	def c_r1 = params.clip_r1 > 0 ? "--clip_r1 ${params.clip_r1}" : ''
   	def c_r2 = params.clip_r2 > 0 ? "--clip_r2 ${params.clip_r2}" : ''
    	def tpc_r1 = params.three_prime_clip_r1 > 0 ? "--three_prime_clip_r1 ${params.three_prime_clip_r1}" : ''
	def tpc_r2 = params.three_prime_clip_r2 > 0 ? "--three_prime_clip_r2 ${params.three_prime_clip_r2}" : ''
	def type = reads.size() == 2 ? '--paired' : ''

	"""
	conda run -n env trim_galore \
	-q 30 \
	${type} \
	--fastqc \
	--gzip \
	-j 4 \
	${c_r1} \
	${c_r2} \
	${tpc_r1} \
	${tpc_r2} \
	${reads}

	mkdir -p "${params.out_dir}/trimfastq"
	cp *.{html,txt} "${params.out_dir}/trimfastq"
	"""
}

// STAR Index
process STAR_INDEX {

	if(params.keep_star_index){

		publishDir "${params.out_dir}", mode: 'copy', overwrite: true
	}
	
	input:
	path genome
	path gff_gtf
	val sjdbOverhang

	output:
	path 'STAR_Index', type: 'dir'

	script:
	def dbxref= params.gff_gtf =~ /\.gff.gz$/ ? "| grep Dbxref" : ''
	def sjdbGTFtagExonParentTranscript= params.gff_gtf =~ /\.gff.gz$/ ? 'Parent' : 'transcript_id'
	if (params.chemistry == "10xv2") {
		sjdbOverhang = 97

	} else if (params.chemistry == "10xv3") {
		sjdbOverhang = 89

	} else if (params.chemistry == "brb") {
		sjdbOverhang = 89
	}

	"""
	gunzip -c ${gff_gtf} ${dbxref} > genome_annot.gff
	gunzip -c ${genome} > genome.fa

	conda run -n env STAR \
		--runMode genomeGenerate \
		--genomeDir ./STAR_Index \
		--genomeFastaFiles genome.fa \
		--sjdbGTFtagExonParentTranscript ${sjdbGTFtagExonParentTranscript} \
		--sjdbGTFfile genome_annot.gff \
		--runThreadN 6 \
		--sjdbOverhang ${sjdbOverhang}
	"""
}

// STAR map
process STAR_MAP {
	tag "${replicateId}"

    publishDir "${params.out_dir}/bam", mode: 'symlink', overwrite: true

	input:
	path genome
	tuple val(replicateId), path(reads)

	output:
	tuple val(replicateId), path('*.bam')

	script:
	"""
	conda run -n env STAR \
		--genomeDir ${genome} \
		--readFilesIn ${reads} \
		--readFilesCommand zcat \
		--outFileNamePrefix "${replicateId}" \
		--outSAMtype BAM SortedByCoordinate \
		--twopassMode Basic \
		--runThreadN 12 \
		--limitSjdbInsertNsj 1200000

	mkdir -p "${params.out_dir}/bam"
	cp *.out "${params.out_dir}/bam"
	"""
}

// remove multimapped reads
process BAM_UNIQUE {
	tag "${replicateId}"

    publishDir "${params.out_dir}/bam", mode: 'symlink', overwrite: true    

	input:
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*.bam')

	script:
	"""
	conda run -n env samtools view -bq 255 \
	-o "${replicateId}Aligned.sortedByCoord.out.unique.bam" \
	${bam}
	"""
}

// HTSEQ
process HTSEQ_COUNT {
	tag "${replicateId}"

	publishDir "${params.out_dir}/HTSEQ_COUNT", mode: 'copy', overwrite: true

	input:
	tuple val(replicateId), path(bam)
	path gff_gtf

	output:
	path '*.count'

	script:
	def i= params.gff_gtf =~ /\.gff.gz$/ ? 'Dbxref' : 'gene_id'
	"""
	gunzip -f -c ${gff_gtf} > genome_annot.gff
    sed -i 's/Dbxref=GeneID:\\(.*\\),Genbank:\\(.*\\);/Dbxref=GeneID:\\1;/' genome_annot.gff

	conda run -n htseq htseq-count \
        -s no \
		-t "${params.feature_type}" \
		-i ${i} \
		-m union \
		-f bam \
		${bam} \
		genome_annot.gff \
        > ${replicateId}.count \
	"""
}

// SALMON Index
process SALMON_INDEX{

	if(params.keep_salmon_index){

		publishDir "${params.out_dir}", mode: 'copy', overwrite: true
	}
	
	input:
	path genome
	path transcriptome

	output:
	path 'SALMON_Index', type: 'dir'

	script:
	"""
	zcat ${genome} | sed 's/ .*\$//g' > genome.fa
	grep "^>"  genome.fa > decoys.txt
	sed -i -e 's/>//g' decoys.txt
	zcat ${transcriptome} | sed 's/ .*\$//g' > transcriptome.fa
	cat transcriptome.fa genome.fa > gentrome.fa

	conda run -n combine salmon index \
		-t gentrome.fa \
		-d decoys.txt \
		-p 12 \
		-i SALMON_Index
	"""
}

// SALMON SAF
process SALMON_SAF {
	tag "${replicateId}"

	publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	input:
	path salmon_index
	tuple val(replicateId), path(reads)

	output:
	path 'SALMON_SAF/*', type: 'dir'

	script:
	def input = params.input ==~ /.*\{.*/ ? "-1 ${reads[0]} -2 ${reads[1]}" : "-r ${reads}"
	def writeMappings = params.writeMappings ? "--writeMappings | samtools view -@ 6 -b -h -O BAM -o SALMON_SAF/${replicateId}/mapped.bam" : ""

	"""
	conda run -n combine salmon quant \
		-i ${salmon_index} \
		-l A \
		-p 6 \
		--gcBias \
		--validateMappings \
		${input} \
		-o "./SALMON_SAF/${replicateId}" \
		${writeMappings}
	"""
}

// ALEVIN SAF
process ALEVIN_SAF {
	tag "${replicateId}"

	publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	input:
	path genome
	tuple val(replicateId), path(reads)

	output:
	path 'ALEVIN_SAF/*', type: 'dir'

	script:
	def input = params.input ==~ /.*\{.*/ ? "-1 ${reads[0]} -2 ${reads[1]}" : "-r ${reads}"
	"""
	conda run -n combine salmon alevin \
		-i SALMON_Index \
		-l ISR \
		-p 10 \
		${input} \
		-o "./ALEVIN_SAF/${replicateId}"
	"""
}

// ALEVIN-fry Index
process ALEVIN_FRY_INDEX{

	if(params.keep_alevin_fry_index){

		publishDir "${params.out_dir}", mode: 'copy', overwrite: true
	}
	
	input:
	path genome
	path gtf

	output:
	path 'ALEVIN_FRY_Index', type: 'dir'

	script:
	if (params.chemistry == "10xv2") {
		rlen = 98

	} else if (params.chemistry == "10xv3") {
		rlen = 90

	} else if (params.chemistry == "brb") {
		rlen = 90
	}
	def type = params.spliceu == true ? "--ref-type spliceu" : ""
	
	"""
	gunzip -c ${gtf} > genome_annot.gtf
	gunzip -c ${genome} > genome.fa

	export ALEVIN_FRY_HOME=.
	conda run -n combine simpleaf set-paths
	conda run -n combine simpleaf index \
		--output ALEVIN_FRY_Index \
		--fasta genome.fa \
		--gtf genome_annot.gtf \
		--threads 16 \
		--use-piscem \
		--rlen ${rlen} \
		${type}
	"""
}

// ALEVIN SAF
process ALEVIN_FRY {
	tag "${replicateId}"

	publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	input:
	path alevin_fry_index
	tuple val(replicateId), path(reads)

	output:
	path 'ALEVIN_FRY/*', type: 'dir'

	script:
	def chemistry = params.chemistry == "brb" ? "1{b[14]u[14]x:}2{r:}" : "${params.chemistry}"
	// auto with simpleaf : 10xv2 --> "1{b[16]u[10]x:}2{r:}"
	// auto with simpleaf : 10xv3 --> "1{b[16]u[12]x:}2{r:}"
	// automatic with simpleaf for 10xv2 and 10xv3 not for brb
	def whitelist = params.chemistry == "brb" ? "--unfiltered-pl ${params.whitelist}" : "--unfiltered-pl"

	"""
	export ALEVIN_FRY_HOME=.
	conda run -n combine simpleaf set-paths
	conda run -n combine simpleaf quant \
		--reads1 ${reads[0]} \
		--reads2 ${reads[1]} \
		--threads 16 \
		--index '${alevin_fry_index}/index' \
		--chemistry ${chemistry} \
		--resolution cr-like \
		--expected-ori fw \
		${whitelist} \
		--t2g-map '${alevin_fry_index}/index/t2g_3col.tsv' \
		--output "./ALEVIN_FRY/${replicateId}"
		#--use-selective-alignment \
	"""
}

// STARSOLO map
process STARSOLO_MAP {
	tag "${replicateId}"

    publishDir "${params.out_dir}/STAR_SOLO", mode: 'symlink', overwrite: true

	input:
	path genome
	tuple val(replicateId), path(reads)

	output:
	tuple val(replicateId), path('*.bam')

	script:
	if (params.chemistry=="10xv2") {
		CB_UMI = "--soloCBstart 1 \
			--soloCBlen 16 \
			--soloUMIstart 17 \
			--soloUMIlen 10"

    } else if (params.chemistry=="10xv3") {
        CB_UMI = "--soloCBstart 1 \
			--soloCBlen 16 \
			--soloUMIstart 17 \
			--soloUMIlen 12"

       } else if (params.chemistry=="brb") {
        CB_UMI ="--soloCBstart 1 \
			--soloCBlen 14 \
			--soloUMIstart 15 \
			--soloUMIlen 14"
    }

	if (params.chemistry ==~ /10xv.*/) {
		filters = "--outFilterScoreMin 30 \
			--soloCBmatchWLtype 1MM_multi_Nbase_pseudocounts \
			--soloUMIdedup 1MM_CR \
			--soloUMIfiltering MultiGeneUMI_CR \
			--soloCellFilter EmptyDrops_CR"

	} else if (params.chemistry == "brb") {
		filters = "--soloUMIdedup 1MM_Directional \
			--soloCBmatchWLtype 1MM \
			--outSAMmapqUnique 60 \
			--soloCellFilter None \
			-outFilterMultimapNmax 1"
	}
	"""
	conda run -n env STAR \
		--runMode alignReads \
		--genomeDir ${genome} \
		--readFilesIn ${reads[1]} ${reads[0]} \
		--readFilesCommand zcat \
		--outFileNamePrefix "${replicateId}" \
		--soloBarcodeReadLength 0 \
		--soloType CB_UMI_Simple \
		${CB_UMI} \
		--soloCBwhitelist ${params.whitelist} \
		--clipAdapterType CellRanger4 \
		${filters} \
		--quantMode GeneCounts \
		--soloFeatures Gene GeneFull SJ Velocyto \
		--soloStrand Forward \
		--outSAMtype BAM SortedByCoordinate \
		--outSAMattributes NH HI nM AS CR UR CB UB GX GN sS sQ sM

	mkdir -p "${params.out_dir}/STAR_SOLO"
	cp -r *.out* "${params.out_dir}/STAR_SOLO"
	"""
}

