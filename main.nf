#!/usr/bin/env nextflow

// Enable DSL 2 syntax
nextflow.enable.dsl = 2

// Import modules 
include { 
	FASTQC;
	TRIM;
	STAR_INDEX;
	STAR_MAP;
	STARSOLO_MAP;
	BAM_UNIQUE;
	HTSEQ_COUNT;
	SALMON_INDEX;
	SALMON_SAF;
	ALEVIN_SAF;
	ALEVIN_FRY_INDEX;
	ALEVIN_FRY
} from "./modules/modules.nf"

// output directory
file("${params.out_dir}").mkdirs()

// main pipeline logic
workflow {

	// fastq input
	reads=Channel.fromPath(params.input)
		.splitCsv(header:true, sep:',')
    	.map {
        	row -> tuple(
            	row.ID,
            	[
					file(row.R1,checkIfExists: true),
					file(row.R2,checkIfExists: true),
				]
        	)
    	}

#	if(params.input ==~ /.*\{.*/){
#	
#		reads=Channel.fromFilePairs(params.input)
#
#	}else{
#		reads=Channel.fromPath(params.input)
#    		.map { file ->
#        		def key = file.name.toString().tokenize('_').get(0)
#       			return tuple(key, file)
#    		}
#	}

	//raw fastQC
	if(params.skip_fastqc==false){

		FASTQC(
			reads
		)
	}

	// 

	// trimming
	if(params.skip_trimming==true | params.chemistry != false){

		tomap_ch=reads
		
	}else{

		TRIM (
			reads
		)

		tomap_ch=TRIM.out

	}

	// STAR_HTSEQ-COUNT
	if(params.method ==~ /.*star_htseq-count.*/){

		// genome index channel
		if(params.star_index ==false){

			STAR_INDEX(
				params.genome,
				params.gff_gtf,
				params.sjdbOverhang	
			)

			star_index  = STAR_INDEX.out

		}else{

			star_index = params.star_index

		}

		// STAR Alignment
		STAR_MAP(
			star_index,
			tomap_ch
		)

		// HTSEQ count with or without multimapped reads
		if(params.htseq_count_multimapped==false){
	
			// keep unikely mapped
			BAM_UNIQUE(
				STAR_MAP.out
			)

			star_bam=BAM_UNIQUE.out

		}else{

			// keep all
			star_bam=STAR_MAP.out
		}

		// HTSEQ count on all maps
		HTSEQ_COUNT(
			star_bam,
			params.gff_gtf
		)
	}

	// SALMON
	if(params.method ==~ /.*salmon_saf.*/){

        // genome index channel
		if(params.salmon_index ==false){

			// Salmon Index
		    SALMON_INDEX(
			    params.genome,
			    params.transcriptome
		    )

			salmon_index  = SALMON_INDEX.out

		}else{

			salmon_index = params.salmon_index

		}

        // map
		SALMON_SAF(
			salmon_index,
			tomap_ch
		)
	}

	// ALEVIN
	if(params.method ==~ /.*alevin_saf.*/){

        // genome index channel
		if(params.salmon_index ==false){

			// Salmon Index
		    SALMON_INDEX(
			    params.genome,
			    params.transcriptome
		    )

			salmon_index  = SALMON_INDEX.out

		}else{

			salmon_index = params.salmon_index

		}

        // map
		ALEVIN_SAF(
			salmon_index,
			tomap_ch
		)
	}

	// ALEVIN FRY
	if(params.method ==~ /.*alevin_fry.*/){

        // genome index channel
		if(params.alevin_fry_index ==false){

			// Salmon Index
		    ALEVIN_FRY_INDEX(
			    params.genome,
				params.gff_gtf
		    )

			alevin_fry_index  = ALEVIN_FRY_INDEX.out

		}else{

			alevin_fry_index = params.alevin_fry_index

		}

        // map
		ALEVIN_FRY(
			alevin_fry_index,
			tomap_ch
		)
	}

	// STAR_SOLO
	if(params.method ==~ /.*star_solo.*/){

		// genome index channel
		if(params.star_index ==false){

			STAR_INDEX(
				params.genome,
				params.gff_gtf,
				params.sjdbOverhang	
			)

			star_index  = STAR_INDEX.out

		}else{

			star_index = params.star_index

		}

		// STAR SOLO Alignment
		STARSOLO_MAP(
			star_index,
			tomap_ch
		)
	}
}
