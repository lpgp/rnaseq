# distribution
    BootStrap: library
    From: ubuntu

%labels
    Maintainer = "Aurelien BRIONNE, INRAE, LPGP unit"
    base.image=ubuntu
    version="1.1"

%environment
    export LC_ALL=C
    export LC_NUMERIC=en_GB.UTF-8
    export PATH="/opt/miniconda/bin:$PATH"

%post
    # minimum install
    apt update -qq
    apt install -y curl wget
    apt upgrade -y
    cd /opt

    # conda install and update
    curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"

    # conda environment tools installation (according environment compatibility)
    conda update -n base -c defaults conda
    conda config --add channels conda-forge
    conda create -c bioconda -c conda-forge --name env trim-galore samtools star
    conda create -c bioconda --name htseq htseq
    conda create -c bioconda -c conda-forge --name combine simpleaf piscem

    # cleanup
    apt autoremove --purge
    apt clean -y
    conda clean -a
    rm -rf /var/log/* /opt/Miniconda3-latest-Linux-x86_64.sh

%runscript
    exec "$@"
