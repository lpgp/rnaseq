#!/usr/bin/env nextflow

// parameters
params{

    // sequences
    input = false
    genome = false
    transcriptome = false

    //fastqc
    skip_fastqc = false

    // trimming
    skip_trimming = false
    clip_r1 = 0
    clip_r2 = 0
    three_prime_clip_r1 = 0
    three_prime_clip_r2 = 0

    // alignment mode
    method = false

    // STAR options
    star_index = false
    gff_gtf = false
    sjdbOverhang = false
    keep_star_index = false
    htseq_count_multimapped = false
    feature_type = "exon"

    // SALMON options
    salmon_index = false
    keep_salmon_index = false
    writeMappings = false

    // ALEVIN FRY options
    alevin_fry_index = false
    keep_alevin_fry_index = false
    chemistry = false
    spliceu = false

    // STAR SOLO & ALEVIN_FRY options
    whitelist = false

    // STAR SOLO options
    star_index = false
    gff_gtf = false
    keep_star_index = false

    // save directory
    out_dir = "${PWD}/results"
}

// profiles
profiles {

    standard {
        process {
            // executor
            executor = 'local'
        }
    }

    slurm {

        process {
            // executor
            executor = 'slurm'

            // queue
            queue = 'workq'
        }
    }
}

// singularity container
singularity{
    enabled = true
//  autoMounts = true
    runOptions = '-B /bank -B /work -B /save -B /home'
}

// process ressources

process{
    
    // Singularity container
    container = "$baseDir/singularity/rnaseq.sif"

    //fastqc
    withName: FASTQC {
        cpus = 2
	    memory = 5.GB
    }

    // trim
    withName: TRIM {
	    cpus = 5
	    memory = 5.GB
    }

    // STAR Index
    withName: STAR_INDEX {
	    cpus = 6
	    memory = 144.GB
    }

    // STAR map
    withName: STAR_MAP {
	    cpus = 12
	    memory = 144.GB
    }

    // STARSOLO map
    withName: STARSOLO_MAP {
	    cpus = 12
	    memory = 144.GB
    }

    // Keep unique mapped
    withName: BAM_UNIQUE {
	    cpus = 12
	    memory = 20.GB
    }

    // HTSEQ_COUNT
    withName:HTSEQ_COUNT {
	    cpus = 12
	    memory = 20.GB
    }

    // SALMON_INDEX
    withName:SALMON_INDEX {
	    cpus = 12
	    memory = 32.GB
    }

    // ALEVIN_SAF
    withName:SALMON_SAF {
	    cpus = 16
	    memory = 20.GB
    }

    // SALMON_SAF
    withName:SALMON_SAF {
	    cpus = 6
	    memory = 20.GB
    }

    // ALEVIN_INDEX
    withName:ALEVIN_FRY_INDEX {
	    cpus = 16
	    memory = 15.GB
    }

    // ALEVIN_FRY
    withName:ALEVIN_FRY {
	    cpus = 16
	    memory = 40.GB
    }
}

// Nextflow trace
trace{
    enabled = true
    overwrite = true
    file = 'rnaseq_trace.txt'
    fields = 'name,status,realtime,%cpu,%mem,rss,vmem,peak_rss,peak_vmem'
}

// Nextflow timeline
timeline{
    enabled = true
    file = "timeline.html"
}

// Nextflow DAG
dag{
    enabled = true
    file = "rnaseq_dag.png"
}
